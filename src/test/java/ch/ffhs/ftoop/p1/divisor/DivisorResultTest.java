package ch.ffhs.ftoop.p1.divisor;

import junit.framework.TestCase;

public class DivisorResultTest extends TestCase {
	/**
	 * Testet getResult Methode von DivisorResult
	 */
	public void testGetResult() {
		DivisorResult r = new DivisorResult(50, 4);
		assertEquals(r.getResult(), 50, 0);
	}
	/**
	 * Testet getCountDiv Methode von DivisorResult
	 */
	public void testGetCountDiv() {
		DivisorResult r = new DivisorResult(50, 4);
		assertEquals(r.getCountDiv(), 4, 0);
	}
	/**
	 * Testet toString Methode von DivisorResult
	 */
	public void testToString() {
		DivisorResult r = new DivisorResult(50, 4);
		String s = "Zahl mit maximaler Anzahl Divisoren: " + 50 + " (" + 4 + " Divisoren)";
		assertEquals(r.toString(), s);
	}
}
