package ch.ffhs.ftoop.p1.divisor;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import student.TestCase;

/**
 * Hinweis: Die Unit Tests haben einen festen Timeout von 10 sekunden - achten
 * Sie daher darauf, dass Sie das Testintervall nicht zu gross gestalten.
 * 
 * @author ble
 * 
 */

public class DivisorThreadTest extends TestCase {
	/**
	 * Tested explizit die Methode "call" aus der Klasse DivisorTest.
	 * 
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	public void testCall() throws ExecutionException, InterruptedException {

		DivisorThread testThread = new DivisorThread(10, 10000);
		ArrayList<Long> result = new ArrayList<>();
		try {
			result = testThread.call();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		assertEquals(result.get(0), (long) 7560, 0);
	}
}
