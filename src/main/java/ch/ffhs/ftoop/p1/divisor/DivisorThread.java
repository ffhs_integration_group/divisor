/**
 * 
 */
package ch.ffhs.ftoop.p1.divisor;

import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Berechnet die Zahl mit den Maximalen Divisoren in einem Teilintervall.
 * @author Yvo Broennimann, Giancarlo Bergamin
 */
public class DivisorThread implements Callable<ArrayList<Long>> {

	private long start;
	private long ende;
	/**
	 * 
	 * @param start Start des Intervalls
	 * @param ende Ende des Intervalls
	 */
	public DivisorThread(long start, long ende) {
		this.start = start;
		this.ende = ende;
	}

	/**
	 * In dieser call Methode wir in dem an den Thread zugewiesenen Intervall
	 * nach der Zahl gesucht, welche die meisten Divisoren hat. Die Zahl und die
	 * Anzahl Divisoren wir dann in einer result ArrayList zurückgegebenen
	 * (Index 0: Zahl mit den meisten Divisoren; Index 1: Anzahl Divisoren)
	 * 
	 */
	@Override
	public ArrayList<Long> call() throws Exception {

		ArrayList<Long> results = new ArrayList<>();

		long meisteDivisoren = start;
		long tmp = 2;

		for (long zahl = start; zahl <= ende; zahl++) {

			long anzahlDivisoren = 2;

			for (long i = 2; i <= zahl / 2; i++) {
				if (zahl % i == 0) {
					anzahlDivisoren++;
				}
			}
			// NICHT >=, so wird wenn es zwei Zahlen mit der gleichen Anzahl
			// Divisoren gibt, die tiefere genommen
			if (anzahlDivisoren > tmp) {
				tmp = anzahlDivisoren;
				meisteDivisoren = zahl;
			}
		}
		results.add(meisteDivisoren);
		results.add(tmp);
		return results;
	}
}
