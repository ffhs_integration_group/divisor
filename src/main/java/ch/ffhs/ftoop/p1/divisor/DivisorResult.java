package ch.ffhs.ftoop.p1.divisor;

/**
 * Hält das Ergebnis einer Berechnung
 * 
 * @author Yvo Broennimann, Giancarlo Bergamin
 * 
 * 
 */
class DivisorResult {
	// das eigentlich ergebnis - die Zahl mit der max. Anzahl von Divisoren
	private long result;

	// Anzahl der Divisoren von Result
	private long countDiv;

	/**
	 * 
	 * @param r
	 *            Zahl mit den meisten Divisoren
	 * @param c
	 *            Anzahl Divisoren
	 */
	public DivisorResult(long r, long c) {
		result = r;
		countDiv = c;
	}

	/**
	 * Gibt die Zahl mit den maximalen Divisoren zurück
	 * 
	 * @return long
	 */
	public long getResult() {
		return result;
	}

	/**
	 * Gibt die Anzahl Divisoren der Zahl mit den meisten Divisoren zurück
	 * 
	 * @return long
	 */
	public long getCountDiv() {
		return countDiv;
	}

	/**
	 * To String Methode ueberschrieben von Object
	 */
	@Override
	public String toString() {
		return "Zahl mit maximaler Anzahl Divisoren: " + result + " (" + countDiv + " Divisoren)";
	}

}