package ch.ffhs.ftoop.p1.divisor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Das folgende Programm soll aus einem vorgegebene Interval von Long-Zahlen die
 * Zahl zurückgeben, die die meisten Divisoren hat. Sollte es mehrere solche
 * Zahlen geben, so soll die kleinste dieser Zahlen ausgegeben werden.
 * 
 * Die Berechnung soll in n Threads stattfinden, die via Executor Framework
 * gesteuert werden, und sich das Problem aufteilen - jeder Thread soll eine
 * Teilmenge des Problems lösen. Verwenden Sie bitte einen FixedThreadPool und
 * implementieren Sie die Worker als Callable.
 * 
 * @author Yvo Broennimann, Giancarlo Bergamin
 * 
 */
public class CalculateDivisor {

	private long von, bis;
	private int threadCount;

	/**
	 * @param von
	 *            untere Intervallgrenze
	 * @param bis
	 *            obere Intervallgrenze
	 * @param threadCount
	 *            Abzahl der Threads, auf die das Problem aufgeteilt werden soll
	 */
	public CalculateDivisor(long von, long bis, int threadCount) {
		this.von = von;
		this.bis = bis;
		this.threadCount = threadCount;
	}

	/**
	 * Berechnungsroutine zur Berechnung der Zahl mit den meisten Divisoren aus einem Intervall. Berechnung mit Multithreading
	 * 
	 * @return Ein DivisoResult Objekt mit dem Berechnungsresultat.
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public DivisorResult calculate() throws InterruptedException, ExecutionException {

		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadCount);
		List<Future<ArrayList<Long>>> resultList = new ArrayList<>();
		
		long anzZahlen = bis - von;
		long chunk = 0;									// Hier wird die Arbeit aufgeteilt (Das Intervall wird so in kleinere Intervalle zerlegt (und zwar soviele Teilintervalle wie es maximal Threads im Threadpool gibt (mann könnte auch eine andere Anzahl an Teilintervalle wählen)))
		if (anzZahlen % threadCount == 0) {				// Falls die Anzahl zu prüfende Zahlen genau durch die Anzahl an Threads teilbar ist, definiere die Variable chunk als Anzahl Zahlen durch Anzahl Threads
			chunk = anzZahlen / threadCount; 
		}else {											// Falls das nicht der Fall ist, ziehe den Rest von der Anzahl Zahlen ab und dividere dann durch die Anzahl Threads 
			chunk = (anzZahlen-(anzZahlen % threadCount)) / threadCount;
		}
		long tmp = von;
		
        for (int i = 1; i <= threadCount; i++) {
        	if (i==threadCount) {						// Für das letzte Teilintevall erweitere das zu überprüfende Teilintervall um den Rest der Division Anzahl Zahlen durch Threadcount
        		DivisorThread worker = new DivisorThread(tmp,tmp+chunk+(anzZahlen % threadCount));	// startet einen Thread und übergibt ihm ein Teilintervall
        		Future<ArrayList<Long>> result = executor.submit(worker);
                resultList.add(result);
                tmp+=chunk;
        	}
        	else {
        		DivisorThread worker = new DivisorThread(tmp,tmp+chunk);
        		Future<ArrayList<Long>> result = executor.submit(worker);
                resultList.add(result);
                tmp+=chunk;
        	}
          }

        HashMap<Long, Long> schlussResultate = new HashMap<>();
        schlussResultate.put((long) 0, (long) 0);
        
        for(Future<ArrayList<Long>> future : resultList)
        {
              try
              {
            	  ArrayList<Long> zwischenresultate = future.get();
            // schaut ob ein Key schon vorhanden ist, es also diese Anzahl Divisoren schon gibt. 
            //Wenn es diese nicht gibt wird es hinzugefügt sonst nicht, da bei gleicher Anzahl Divisoren die kleinere Zahl genommen wird
            	  if(!schlussResultate.keySet().contains(zwischenresultate.get(1)) 
            			  && zwischenresultate.get(0) > Collections.max(schlussResultate.values())) {
            		  schlussResultate.put(zwischenresultate.get(1), zwischenresultate.get(0));
            	  }
              } 
              catch (InterruptedException | ExecutionException e) 
              {
                  e.printStackTrace();
              }
          }
        long maxDiv = Collections.max(schlussResultate.keySet());
        long maxDivZahl = schlussResultate.get(maxDiv);
        
        executor.shutdown();

		return new DivisorResult(maxDivZahl, maxDiv);
	}
	/**
	 * Main Methode. Es muss als Argument "intervalStart" "intervalEnd" "threadCount" mitgegeben werden
	 * @param args
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public static void main(String[] args) throws InterruptedException,
			ExecutionException {
		if (args.length != 3) {
			System.out
					.println("Usage: CalculateDivisor <intervalStart> <intervalEnd> <threadCount>");
			System.exit(1);
		}
		
		
		long von = Long.parseLong(args[0]);
		long bis = Long.parseLong(args[1]);
		int threads = Integer.parseInt(args[2]);

		if (von>bis){							//testet ob die Intervallgrenzen reichtig gesetzt wurden (die untere Grenze zu erst und die obere als zweites Argument)
			System.out.println("Start of interval can't be bigger than end of interval.");
			System.exit(1);
		}
		
		if (threads <= 0){						//testet ob die Eingabe zur ANzhal Thread in Ordnung ist (muss grösser als 0 sein)
			System.out.println("Threadcount must be bigger than 0.");
			System.exit(1);
			
		}
		
		CalculateDivisor cp = new CalculateDivisor(von, bis, threads);
		System.out.println("Ergebnis: " + cp.calculate());
	}

}
